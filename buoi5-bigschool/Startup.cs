﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(buoi5_bigschool.Startup))]
namespace buoi5_bigschool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
